
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insecao {
    public void inserirCliente(String nomec,String telefone,String idade,String cpf,String rg ){
        try {
            Connection c = Conexao.ObterConexao();
            System.out.println(c);
            PreparedStatement ps=c.prepareStatement("INSERT INTO SC_BAR.CLIENTE(nomec,telefone,idade,cpf,rg) "
                    + "VALUES(?,?,?,?,?)");
            ps.setString(1,nomec);
            ps.setInt(2, Integer.valueOf(telefone));
            ps.setInt(3,Integer.valueOf(idade));
            ps.setInt(4,Integer.valueOf(cpf));        
            ps.setInt(5,Integer.valueOf(rg));
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Insecao.class.getName()).log(Level.SEVERE, null, ex);
        }
                }
    public void inserirProprietario(String nomep,String telefonep,String cnpj,String cpfp,String rgp ) throws SQLException{
            Connection c = Conexao.ObterConexao();
            System.out.println(c);
            PreparedStatement ps=c.prepareStatement("INSERT INTO sc_bar.proprietario(nomep,telefonep,cnpj,cpfp,rgp) "
                    + "VALUES(?,?,?,?,?)");
            ps.setString(1,nomep);
            ps.setInt(2, Integer.valueOf(telefonep));
            ps.setInt(3,Integer.valueOf(cnpj));
            ps.setInt(4,Integer.valueOf(cpfp));        
            ps.setInt(5,Integer.valueOf(rgp));
            ps.execute();
        }
    public void inserirEstabelecimento(String nome,String endereco,String cnpj,String cpfp,String telefone ) throws SQLException{
            Connection c = Conexao.ObterConexao();
            System.out.println(c);
            PreparedStatement ps=c.prepareStatement("INSERT INTO sc_bar.estabelecimento(nome,endereco,cnpj,cpfp,telefone) "
                    + "VALUES(?,?,?,?,?)");
            ps.setString(1,nome);
            ps.setString(2,endereco);
            ps.setString(3,cnpj);
            ps.setInt(4,Integer.valueOf(cpfp));        
            ps.setString(5,telefone);
            ps.execute();
        }
     
}
