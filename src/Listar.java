
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author aluno
 */
public class Listar {

    public void selecaoCliente(JComboBox combo) {
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.ObterConexao();
            PreparedStatement p = c.prepareStatement("Select * from SC_bar.Cliente");
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Cliente a = new Cliente(rs.getString("cpf"), rs.getString("nomec"));
                m.addElement(a);
            }
            combo.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listar.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void listarCliente(JList listaCliente) throws SQLException {
        listaCliente.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = Conexao.ObterConexao();
        String SQL = "SELECT * FROM SC_bar.cliente";
        PreparedStatement ps = c.prepareStatement(SQL);
        Statement s = c.createStatement();
        //ResultSet rs = ps.executeQuery(SQL);
        ResultSet rs = s.executeQuery(SQL);
        while (rs.next()) {
            dfm.addElement(rs.getString("nomec"));
        }
        listaCliente.setModel(dfm);
        c.close();
    }

    public void listarEstabelecimento(JList listaProprietario) throws SQLException {
        listaProprietario.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = Conexao.ObterConexao();
        String SQL = "SELECT * FROM SC_bar.proprietario";
        PreparedStatement ps = c.prepareStatement(SQL);
        Statement s = c.createStatement();
        //ResultSet rs = ps.executeQuery(SQL);
        ResultSet rs = s.executeQuery(SQL);
        while (rs.next()) {
            dfm.addElement(rs.getString("nomep"));
        }
        listaProprietario.setModel(dfm);
        c.close();
    }

}
